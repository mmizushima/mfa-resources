# README #

This is a repository of multi-factor authentication(MFA) configuration snippets, templates, and quick task-oriented guides for various products that the CSU has assembled for sharing and recycling amongst member campuses.

### Repository Purpose ###

There are configuration recipes for tools commonly integrated with MFA throughout the CSU.

+ Basic technical needs

    * Getting a Duo account: The CSU Chancellor's office has acquired a master Duo site license.  To establish your instance, please create a Duo administrative account by going to https://duo.com/ and creating an account with an email address in your domain.  Then, please contact Duo and work with them to associate your new account with the CSU master contract.

    * Our MFA Services and Descriptions:  The CO has defined a single string to represent use of MFA in access transactions throughout the CSU, `https://iam.calstate.edu/csuconnect/mfa`.  This string should be sent by all applications that require MFA.  Applications may provide additional signaling mechanisms if need be.  Identity providers should return the same authentication context in any transaction where the user was authenticated with two factors.  The CO does not verify that this string has been sent and the campus is responsible for demonstrating that MFA was used in any circumstances.

    * Our MFA Technical Basics: The only MFA service offered by the CSU today is located at `https://login.calstate.edu/csuconnect/cfsmfa.asp?env=FCFSPRD`.  As more are added, a service catalogue will be built.

    * CO-offered services requiring MFA will use the entityID `https://login.calstate.edu/mfa`.

+ MFA Configuration Examples for Products
    * Shibboleth
    * Okta
    * Shibboleth / CAS Integration
    * CAS

All CSU campuses are invited to submit issues or pull requests for MFA technical tasks that they would like to introduce for systemwide visibility or collaboration and for completed guides.

### Who do I talk to? ###

* Nate Klingenstein (nklingenstein@calstate.edu) coordinates technical efforts for the MFA project